buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin("gradle-plugin:1.3.50"))
    }
}

allprojects {
    repositories {
        mavenCentral()
        jcenter()
    }
}